import sources
import sinks


def source1():

    x=0
    while x == 0:
        source = input("Dime una fuente (load, sin, constant, square): ")

        if source == 'sin':
            nsamples = input('¿Parametros de nsamples para sin?: ')
            freq = float(input('¿Frecuencia para sin?: '))
            sound = sources.sin(int(nsamples), int(freq))
            x=1


        elif source == 'constant':
            nsamples = input('¿NUmeros de muestras para constant?: ')
            level = int(input('¿Valor de nivel para constant?: '))
            sound = sources.constant(int(nsamples), int(level))
            x=1

        elif source == 'square':
            nsamples = int(input('¿Número de muestras para square?: '))
            nperiod = int(input('¿Periodo de square?: '))
            sound = sources.square(int(nsamples), int(nperiod))
            x=1

        if source == 'load':
            str =input('¿Nombre del fichero de audio para load?: ')
            sound = sources.load(str)
            x=1

    while x ==1:

        sink= input('Dime un sumidero (play, draw, show, info): ')

        if source == 'sin':
            print(f'Ejecutando sin', nsamples , freq , 'y', sink)

        if source == 'constant':
            print(f'Ejecutando constant', nsamples , level , 'y' , sink)

        if source == 'square':
            print(f'Ejecutando square', nsamples , nperiod , 'y',sink)

        if source == 'load':
            print(f'Ejecutando load y ',sink)


        if sink == 'play':
            sinks.play(sound)
            x=0

        if sink == 'draw':
            maximo_character = int(input('¿Maximo numero para draw?: '))
            sinks.draw(sound, maximo_character)
            x=0

        if sink == 'show':
            newline = input("¿Separados por comas?(True or false)")
            if newline == 'True':
                sinks.show(sound,newline=True)
            else:
                sinks.show(sound,newline=False)
            x = 0


        if sink == 'info':
            sinks.info(sound)
            x=0


def main():
    source1()


if __name__ == '__main__':
    main()