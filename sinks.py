"""Methods for storing sound, or playing it"""

import array
import config

import sounddevice


def play(sound):
    sound_array = array.array('h', [0] * len(sound))
    for nsample in range(len(sound)):
        sound_array[nsample] = sound[nsample]
    sounddevice.play(sound_array, config.samples_second)
    sounddevice.wait()

def draw(sound, max_character: int):
    total_chars = max_character * 2
    for nsample in range(len(sound)):
        if sound[nsample] > 0:
            print(' ' * max_character, end='')
            x= int(sound[nsample])
            nchars = int(max_character * (x / config.max_amp))
            print('*' * nchars)
        else:
            nchars = int(max_character * (-sound[nsample] / config.max_amp))
            print(' ' * (max_character - nchars), end='')
            print('*' * nchars)

def show(sound, newline: bool):

    for nsamples in sound:

        if newline == False:
            print(nsamples, end=", ")
        else:
            print(f"{nsamples}\n")

def info(sound):

    media = sum(sound)/len(sound)
    valorposit=0
    valornegat=0
    valornul=0
    for i in(sound):
        if i > 0:
            valorposit +=1
        elif i < 0:
            valornegat +=1
        else:
            valornul += 1

    print(f'samples: {len(sound)}\nMax value: {max(sound)} (sample: {len(sound) - sound[::-1].index(max(sound)) - 1})\n'
          f'Min value: {min(sound)} (samples: {len(sound) - sound[::-1].index(min(sound)) - 1})\nMean value: {media}\n'
          f'positive samples: {valorposit}\nnegative samples: {valornegat}\nnull samples: {valornul}')
        #buscamos el valor maximo de la lista no invertida, pero luego se la restamos a la normal, por que piden la posicion del ultimo valor maximo:al darle esta vuelta a la lista, tenemos que restar 1 tambien, ya que la lista de normal tiene como primera posicion el 0 y si no no se tendria en cuenta.(Igual para el valor minimo)