"""Methods for processing sound (sound processors)"""

import math

import soundfile

import config

def ampli(sound, factor: float):
    """Amplify signal (sound) by the specified factor

    :param sound:  sound (list of samples) to amplify
    :param factor: amplification factor
    :return:     list of samples (sound)
    """

    out_sound = [0] * len(sound)
    for nsample in range(len(sound)):
        value = sound[nsample] * factor
        if value > config.max_amp:
            value = config.max_amp
        elif value < - config.max_amp:
            value = - config.max_amp
        sound[nsample] = int(value)
    return sound

def shift(sound, value:int):

    for nsample in range(len(sound)):
        valordesplazado= sound[nsample] + value

        if valordesplazado > config.max_amp:
            valordesplazado = config.max_amp
        elif valordesplazado < -config.max_amp:
            valordesplazado = -config.max_amp

        sound[nsample] = valordesplazado #cambia todas las muestras de sound por el nuevo valor desplazado
    return sound



def trim(sound,reduction:int,start:bool):
    if reduction >= len(sound):
        sound = []
        return sound

    if start == True:
        del sound[:reduction]

    else:
        del sound[-reduction:]
    return sound

def repeat(sound, factor:int):

    sound = sound * factor
    return sound

def clean (sound,level:int):
    for nsample in range (len(sound)):
        if sound[nsample] > level or sound[nsample] < -level:
            sound[nsample]=sound[nsample]  #todos los que esten fuera del intervalo(-level,level) se quedan como estaban
        else:
            sound[nsample]= 0  #los que estan dentro del intervalo pasan a ser 0
    return sound

def add(sound1,sound2):
    sumarsound = []

    if len(sound1) > len(sound2):
        minsound = sound2
        maxsound = sound1
    if len(sound1) < len(sound2):
        minsound = sound1
        maxsound = sound2

    for i in range(len(minsound)):
        l = minsound[i] + maxsound[i]
        sumarsound.append(l)
        minsound[i] = sumarsound[i]
        if abs(minsound[i]) > config.max_amp:
            minsound[i] = config.max_amp
        if abs(minsound[i]) < -config.max_amp:
            minsound[i] = - config.max_amp
        sound = minsound + maxsound[len(minsound):]
    return sound
