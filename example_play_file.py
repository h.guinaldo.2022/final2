#!/usr/bin/env python3

import sources
import sinks

def main():
    sound = sources.load('recording.wav')
    sinks.play(sound)

if __name__ == '__main__':
    main()