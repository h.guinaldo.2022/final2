"""Programa que acepta como argumentos una fuente, un procesador y un sumidero
"""

import sys

import sources
import sinks
import processors


def main():
    source = sys.argv[1]
    source_arg1 = sys.argv[2]

    if source == 'sin':

        source_arg2 = sys.argv[3]
        sound = sources.sin(nsamples=int(source_arg1), freq=float(source_arg2))
        x = 3

    elif source == 'constant':

        source_arg2 = sys.argv[3]
        sound = sources.constant(nsamples=int(source_arg1), level=int(source_arg2))
        x = 3

    elif source == 'square':

        source_arg2 = sys.argv[3]
        sound = sources.square(nsamples=int(source_arg1), nperiod=int(source_arg2))
        x = 3

    elif source == 'load':

        sound = sources.load(path=source_arg1)
        x = 2

    else:

        sys.exit('Error')


    for i in sys.argv:
        opcion1 = sys.argv[x + 1]
        if opcion1 == 'ampli':

            processor_arg1 = sys.argv[x + 2]
            processors.ampli(sound, factor=float(processor_arg1))
            x += 2

        elif opcion1 == 'shift':

            processor_arg1 = sys.argv[x + 2]
            processors.shift(sound, value=int(processor_arg1))
            x += 2

        elif opcion1 == 'trim':

            processor_arg1 = sys.argv[x + 2]
            processor_arg2 = eval(sys.argv[x + 3])
            sound = processors.trim(sound, reduction=int(processor_arg1), start=bool(processor_arg2))
            x += 3

        elif opcion1 == 'repeat':

            processor_arg1 = sys.argv[x + 2]
            sound = processors.repeat(sound, factor=int(processor_arg1))
            x += 2

        elif opcion1 == 'clean':
            processor_arg1 = sys.argv[x+2]
            processors.clean(sound,level=int(processor_arg1))
            x += 2

        elif opcion1 == 'play':

            sinks.play(sound)
            x+=1

        elif opcion1 == 'draw':

            sink_arg1 = sys.argv[x + 2]
            sinks.draw(sound, max_chars=int(sink_arg1))
            x+=2

        elif opcion1 == 'show':

            sink_arg1 = eval(sys.argv[x + 2])
            sinks.show(sound, newline=bool(sink_arg1))
            x+=2

        elif opcion1 == 'info':

            sinks.info(sound)
            x+=1

        if x+1 == len(sys.argv):
            break


if __name__ == '__main__':
    main()