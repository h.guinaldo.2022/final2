import sources
import sinks
import processors

def source1():

    x=0 #variable que permite que el programa empiece
    while x == 0:
        source = input("Dime una fuente (load, sin, constant, square): ")

        if source == 'sin':
            nsamples = input('¿Parametros de nsamples para sin?: ')
            freq = float(input('¿Frecuencia para sin?: '))

            sound = sources.sin(int(nsamples), int(freq))
            x=1 #para que pase de los sources a los processors


        elif source == 'constant':
            nsamples = input('¿Número de muestras para constant?: ')
            level = int(input('¿Valor de nivel para constant?: '))

            sound = sources.constant(int(nsamples), int(level))
            x=1

        elif source == 'square':
            nsamples = int(input('¿Número de muestras para square?: '))
            nperiod = int(input('¿Periodo para square?: '))

            sound = sources.square(int(nsamples), int(nperiod))
            x=1

        if source == 'load':
            str =input('¿Nombre del fichero de audio para load?: ')

            sound = sources.load(str)
            x=1

    while x==1:
        processor= input("Dime un processor(ampli,shift,trim,repeat,clean,add): ")

        if processor == 'ampli':
            factor = float(input("¿factor para ampli?"))
            processors.ampli(sound, factor)
            x=2 #para que pase de los processors a los sinks

        if processor == 'shift':
            value=int(input("¿valor de desplazamiento para shift?"))
            processors.shift(sound,value)
            x=2

        if processor == 'trim':
            reduction= int(input("¿valor reduction para trim?"))
            start = eval(input("¿Quitar las muestras del principio(True) o del final (False) para trim?"))
            sound = processors.trim(sound,reduction,start)
            x = 2

        if processor == 'repeat':
            factor= int(input("¿valor factor de repeticion para repeat?"))
            sound = processors.repeat(sound,factor)
            x = 2

        if processor == 'clean':
            level = int(input("¿valor level para clean?"))
            processors.clean(sound,level)
            x = 2

        if processor == 'add':
            sound2 = []
            x =True
            while x:
                accion = input("¿Quieres seguir introducir(True) o parar(False)?")
                if accion == 'True':
                    i = int(input("¿Que quieres introducir en la lista?"))
                    sound2.append(i)

                if accion == 'False':
                    sound = processors.add(sound1 = sound, sound2=sound2)
                    break
            x = 2


    while x ==2:
        sink= input('Dime un sumidero (play, draw, show, info): ')
        if source == 'sin':
            print(f'Ejecutando sin', nsamples, freq, 'con', processor, 'y', sink)

        if source == 'constant':
            print(f'Ejecutando constant', nsamples, level, 'con', processor, 'y', sink)

        if source == 'square':
            print(f'Ejecutando square', nsamples, nperiod, 'con', processor, 'y', sink)

        if source == 'load':
            print(f'Ejecutando load y ', 'con', processor, 'y', sink)


        if sink == 'play':
            sinks.play(sound)
            break

        if sink == 'draw':
            maximo_character = int(input('¿Maximo numero para draw? '))
            sinks.draw(sound, maximo_character)
            break

        if sink == 'show':
            newline = input("¿Separados por comas?(True or False)?")
            if newline == 'True':
                sinks.show(sound,newline=True)
            else:
                sinks.show(sound,newline=False)
            break

        if sink == 'info':
            sinks.info(sound)
            break


def main():
    source1()

if __name__ == '__main__':
    main()