import sources
import sinks


def source1():

    x=0
    while x == 0:
        source = input("Dime una fuente (load, sin, constant, square): ")

        if source == 'sin':
            correcta=True
            while correcta:
                try:
                    nsamples = int(input('¿Parametros de nsamples para sin?: '))
                    freq = float(input('¿Frecuencia para sin?: '))

                    sound = sources.sin(int(nsamples), int(freq))
                    correcta=False
                except:
                    print("Error, los datos tienen que ser enteros")
            x=1


        elif source == 'constant':
            correcta = True #asignamos la variable booleana para poder introducirla en el bucle while
            while correcta:
                try:
                    nsamples = int(input('¿Número de muestras para constant?: '))
                    level = int(input('¿Valor de nivel para constant?: ')) #ponemos el tipo de cada parametro que introducimos al preguntarlo para que si no concuerdan salten directamente al error

                    sound = sources.constant(int(nsamples), int(level))
                    correcta = False #para terminar con el bucle
                except:
                    print("Error, los datos tienen que ser enteros")

            x=1

        elif source == 'square':
            correcta = True
            while correcta:
                try:
                    nsamples = int(input('¿Número de muestras para square?: '))
                    nperiod = int(input('¿Periodo de square?: '))

                    sound = sources.square(int(nsamples), int(nperiod))
                    correcta = False
                except:
                    print("Error, los datos tienen que ser enteros")

            x=1

        if source == 'load':
            correcta = True
            while correcta:
                try:
                    str = input('¿Nombre del fichero de audio para load?: ')

                    sound = sources.load(str)
                    correcta = False
                except:
                    print("Error, el dato tienen que ser un string")

            x=1

    while x ==1:

        sink= input('Dime un sumidero (play, draw, show, info): ')

        if source == 'sin':
            print(f'Ejecutando sin', nsamples , freq , 'y', sink)

        if source == 'constant':
            print(f'Ejecutando constant', nsamples , level , 'y' , sink)

        if source == 'square':
            print(f'Ejecutando square', nsamples , nperiod , 'y',sink)

        if source == 'load':
            print(f'Ejecutando load y ',sink)


        if sink == 'play':
            sinks.play(sound)
            x=0

        if sink == 'draw':
            correcta = True
            while correcta:
                try:
                    maximo_character = int(input('¿Maximo numero para draw:? '))
                    sinks.draw(sound, maximo_character)
                    correcta = False
                except:
                    print("Error, los datos tienen que ser enteros")


            x=0

        if sink == 'show':
            correcta = True
            while correcta:
                try:
                    newline = bool(input("¿Separados por comas?(True or false)"))
                    if newline == 'True':
                        sinks.show(sound, newline=True)
                    else:
                        sinks.show(sound, newline=False)
                    correcta = False
                except:
                    print("Error, el dato tiene que ser True o False")

            x = 0


        if sink == 'info':
            sinks.info(sound)

            x=0



def main():
    source1()


if __name__ == '__main__':
    main()