#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import unittest

import sources

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')


class TestSin(unittest.TestCase):

    def test_simple(self):
        sound = sources.sin(100, 400)
        self.assertEqual(100, len(sound))
        self.assertEqual(0, sound[0])
        self.assertEqual(1139, sound[1])
        self.assertEqual(10791, sound[10])


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
