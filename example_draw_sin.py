#!/usr/bin/env python3

import sources
import sinks

def main():
    sound = sources.sin(41000, 600)
    sinks.draw(sound, 30)

if __name__ == '__main__':
    main()